# DVB-T stations in Greece
* UHF channel allotments in [DIGEA](http://www.digea.gr/234/article/1854/Xartis-suxnotiton/en)
* List of Greece tv station frequencies in [wikipedia](https://el.wikipedia.org/wiki/%CE%9A%CE%B1%CF%84%CE%AC%CE%BB%CE%BF%CE%B3%CE%BF%CF%82_%CF%84%CE%B7%CE%BB%CE%B5%CE%BF%CF%80%CF%84%CE%B9%CE%BA%CF%8E%CE%BD_%CF%83%CF%84%CE%B1%CE%B8%CE%BC%CF%8E%CE%BD_%CF%84%CE%B7%CF%82_%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1%CF%82#Allotment_20_-_.CE.9C.CE.B7.CF.84.CF.81.CE.BF.CF.80.CE.BF.CE.BB.CE.B9.CF.84.CE.B9.CE.BA.CE.AE_.CE.A0.CE.B5.CF.81.CE.B9.CE.BF.CF.87.CE.AE_.CF.84.CE.B7.CF.82_.CE.91.CE.B8.CE.AE.CE.BD.CE.B1.CF.82.5B25.5D)

gr-Greece initial scan file contains all frequencies in Greece and is created with bash script:
```
for c in `seq 21 60`;                  do echo T $((306+($c*8)))000000 8MHz 3/4 NONE QAM64 8k 1/8 NONE; done >gr-Greece
```
gr-Athens initial scan file contains all freequencies in alotment 20 created with bash script:
```
for c in `21 22 27 28 30 40 45 52 54`; do echo T $((306+($c*8)))000000 8MHz 3/4 NONE QAM64 8k 1/8 NONE; done >gr-Athens
```
File channels.conf  created with 'scan' command from [dvb-apps](https://www.linuxtv.org/wiki/index.php/LinuxTV):
```
scan gr-Greece
```
